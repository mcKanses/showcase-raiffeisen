let counter = 0;

while (counter++ < 100) {
  console.log(
    (counter % 3 === 0 || counter % 5 === 0)
      ? (counter % 3 === 0 ? 'Fizz' : '') + (counter % 5 === 0 ? 'Buzz' : '')
      : counter
  );

}
