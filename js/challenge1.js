(async () => {
  
  try {
    
    // PART 1 of challenge
    const postsUrl = `https://jsonplaceholder.typicode.com/posts/`;
    
    const response = await fetch(postsUrl);

    if (response.status !== 200) throw new Error('error while fetching data from source');
  
    const posts = await response?.json?.();

    const filteredObjects = posts.filter(({ body }) => body.includes('sint') && body.length > 150);

    // output filtered objects
    console.log(filteredObjects);

    // PART 2
    let mails = [];
    
    for (let post of filteredObjects) {

      const commentsUrl = `https://jsonplaceholder.typicode.com/comments/${post.id}`;

      const response = await fetch(commentsUrl);

      if (response.status !== 200) throw new Error('error while fetching data from source');
      
      const comments = await response?.json?.();;
    
      mails.push(comments?.email);
      
    };
    
    mails = mails.filter((value, index, array) => array.indexOf(value) === index);
    
    // output result
    console.log(mails);


  } catch (error) {

    console.error(error);
    
  }

})();  
